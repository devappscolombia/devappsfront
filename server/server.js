const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

// console.log that your server is up and running

// create a GET route
app.get('/express_backend', (req, res) => {
  res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' });
});

app.use('/public', express.static('public'));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
    var  fs = require('fs');
    app.get('/es', function (req, res) {
        var filePath = "/public/portafolio_Devapps.pdf";

        fs.readFile(__dirname + filePath , function (err,data){
            res.contentType("application/pdf");
            res.send(data);
        });
    });

    app.get('/en', function (req, res) {
      var filePath = "/public/Devapps_portfolio.pdf";

      fs.readFile(__dirname + filePath , function (err,data){
          res.contentType("application/pdf");
          res.send(data);
      });
  });
    app.listen(port, () => console.log(`Listening on port ${port}`));

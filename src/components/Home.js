import React, {Component} from 'react';

class Home extends Component{
	render(){
		return(
			<section id="home" className="home bg-black fix">
                <div className="overlay"></div>
                <div className="container bg-mod">
                    <div className="row">
                        <div className="main_home">
                            <div className="col-md-12">
                                <div className="hello_slid">
                                    <div className="slid_item xs-text-center">
                                        <div className="col-sm-8 container-text">
                                            <div className="home_text xs-m-top-30">
                                                    <h2 className="text-white shadow-text">{this.props.t('Welcome')}<strong className="strong-text">Devapps</strong></h2>
                                                    <h1 className="text-white shadow-text">{this.props.t('PhrasePart1')}</h1>
                                                    <h3 className="text-white shadow-text">{this.props.t('PhrasePart2')}</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		);
	}
}

export default Home;
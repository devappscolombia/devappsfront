import React, {Component} from 'react';
import config from '../config/config';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import '../App.css';
import {
    Link
  } from "react-router-dom";
const styles = {
    formControl: {
        margin: 1,
        minWidth: 40,
      },
      selectEmpty: {
        marginTop: 2,
      },
};

class Navigation extends Component {
    constructor(){
        super();
        this.state={
            language: 'en'
        }
    }
    
    handleChangeLanguage = event => {
        this.setState({ language: event.target.value});
        switch(event.target.value){
            case "es":
                this.props.changeLanguage('es');
               break;
            case "en":
                this.props.changeLanguage('en');
                break
        }
    }

	render(){
        const Portafolio = ({ language }) => {
            switch(language){
                case "es":
                    return <li><a href={config.server_ip + "/public/portafolio_Devapps.pdf" } target="_blank">Portafolio</a></li>
                case "en":
                    return <li><a href={config.server_ip + "/public/Devapps_portfolio.pdf" } target="_blank">Portfolio</a></li>
            }
        }

		return(
			<nav className="navbar navbar-default bootsnav navbar-fixed no-background white">
                <div className="container"> 
            
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i className="fa fa-bars"></i>
                        </button>
                        <a className="navbar-brand img-logo-mod" href="#brand">
                            <img src="assets/images/logo.png" className="logonav" alt=""/>
                        </a>
                        <div className="languages">
                            {/*<button onClick={() => this.props.changeLanguage('es')}><img src="assets/images/spain-flag-square-icon-16.png" alt="" /></button>
                            <button onClick={() => this.props.changeLanguage('en')}><img src="assets/images/united-kingdom-flag-square-icon-16.png" alt="" /></button>*/}
                        <FormControl className={this.props.classes.formControl} >
                                <Select value={this.state.language} onChange={this.handleChangeLanguage} className={this.props.classes.selectEmpty}>
                                    <MenuItem value={'es'}><img src="assets/images/spain-flag-square-icon-16.png" alt="" /></MenuItem>
                                    <MenuItem value={'en'}><img src="assets/images/united-kingdom-flag-square-icon-16.png" alt="" /></MenuItem>   
                                </Select>
                        </FormControl>
                        </div>
                    </div>
                   
                    <div className="collapse navbar-collapse" id="navbar-menu">
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="#home">{this.props.t('Start')}</a></li>                    
                            <li><a href="#features">{this.props.t('About')}</a></li>
                            <li><a href="#product">{this.props.t('Products')}</a></li>
                            <li><a href="#team">{this.props.t('Team')}</a></li>
                            <li><a href="#contact">{this.props.t('Contact')}</a></li>
                            <li><Link to={{ pathname: "/portafolio", state: { language: this.state.language} }}>{this.props.t('Portafolio')}</Link></li>       
                        </ul>
                    </div>
                </div> 

            </nav>
		);
	}
}

export default withStyles(styles)(Navigation);
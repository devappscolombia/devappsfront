import React, {Component} from 'react';

class Business extends Component{
	render(){
		return(
			<section id="business" className="business bg-blue roomy-70">
                <div className="business_overlay"></div>
                <div className="container">
                    <div className="row">
                        <div className="main_business">
                            <div className="col-md-5">
                                <div className="business_item sm-m-top-50">
                                    <h2 className="text-uppercase text-white"><strong>Made</strong> is Template For Business</h2>

                                    <p className="m-top-20 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit pellentesque eleifend in mi 
                                        sit amet mattis suspendisse ac ligula volutpat nisl rhoncus sagittis cras suscipit 
                                        lacus quis erat malesuada lobortis eiam dui magna volutpat commodo eget pretium vitae
                                        elit etiam luctus risus urna in malesuada ante convallis.</p>

                                    <ul className="text-white">
                                        <li><i className="fa fa-arrow-circle-right"></i> Clean & Modern Design</li>
                                        <li><i className="fa  fa-arrow-circle-right"></i> Fully Responsive</li>
                                        <li><i className="fa  fa-arrow-circle-right"></i> Google Fonts</li>
                                    </ul>

                                    <div className="business_btn m-top-50">
                                        <a href="" className="btn btn-default m-top-20">Buy This Template</a>
                                    </div>

                                </div>
                            </div>

                            <div className="col-md-7">
                                <div className="business_item">
                                    <div className="business_img">
                                        <img src="assets/images/business-img.png" alt="" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
		);
	}
}

export default Business;


import React from 'react'
import PDFViewer from 'pdf-viewer-reactjs'
 
const Portafolio = (props) => {
    console.log(props.location.state.language);
    switch(props.location.state.language){
        case "es":
            return (
                <PDFViewer
                document={{
                    url: 'https://arxiv.org/pdf/quant-ph/0410100.pdf',
                }}
            />
            );
        case "en":
            return (
                <PDFViewer
                document={{
                    url: 'https://arxiv.org/pdf/quant-ph/0410100.pdf',
                }}
            />
            ); 
    }
}
 
export default Portafolio
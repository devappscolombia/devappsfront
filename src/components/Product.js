import React, {Component} from 'react';

class Product extends Component {
	render(){
		return(
			<section id="product" className="product text-center main_product_bg">
                <div className="container">
                    <div className="main_product roomy-80 our-work">
                        <div className="head_title text-center fix">
                        <h2 className="text-uppercase text-black">{this.props.t('Our work')}</h2>
                            <h5>{this.props.t('Our Work Subtitle')}</h5>
                        </div>
                        
                        <div className="main-work">
                            <div className="row works">
                                <div className="col element-work">
                                    <div className="port_item xs-m-top-30">
                                        <div className="port_img">
                                            <img src="assets/images/tiendadetrabajos.jpg" alt="" />
                                        </div>
                                        <div className="port_caption m-top-20">
                                            <h5>{this.props.t('FirstApp')}</h5>
                                            <h6>e-commerce</h6>
                                        </div>
                                    </div>
                                </div>

                                <div className="col element-work">
                                    <div className="port_item xs-m-top-30">
                                        <div className="port_img">
                                            <img src="assets/images/servixion.jpg" alt="" />
                                        </div>
                                        <div className="port_caption m-top-20">
                                            <h5>{this.props.t('SecondApp')}</h5>
                                            <h6>aplicación</h6>
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                            </div>
                    </div>
                </div>
            </section>
		);
	}
}

export default Product;
import React, {Component} from 'react';

class Clients extends Component{
    render(){
        return(
            <section id="test" className="test bg-black roomy-60 fix">
                <div className="overlay"></div>
                <div className="container">
                    <div className="row">                        
                        <div className="main_test fix text-center">

                            <div className="col-md-12 col-sm-12 col-xs-12">
                                <div className="head_title text-center fix">
                                <h2 className="text-uppercase text-white">{this.props.t('Our Costumers')}</h2>
                                    <h5 className="text-white">{this.props.t('Testimonials')}</h5>
                                </div>
                            </div>

                            <div id="testslid" className="carousel slide carousel-fade" data-ride="carousel">

                                <div className="carousel-inner" role="listbox">
                                    <div className="item active">
                                        <div className="col-md-8 col-md-offset-2">
                                            <div className="test_item fix">
                                                <div className="test_img fix">
                                                    <img className="img-circle img-circle-clients" src="assets/images/traigo.jpg" alt="" />
                                                </div>

                                                <div className="test_text text-white">
                                                    <em>{this.props.t('Testimonial Description')}</em>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Clients;
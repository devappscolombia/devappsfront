import React, {Component} from 'react';

import {members} from '../data/members.json'

class Team extends Component{

	constructor(){
		super();
		this.state = {members: members}
	}

	render(){
		return(
			<section id="team" className="team bg-grey fix main_team_bg">
                <div className="container">
                    <div className="main_team roomy-80">
                        <div className="head_title text-center fix">
                            <h2 className="text-uppercase">{this.props.t('Human talent')}</h2>
                            <h5>{this.props.t('Human talent description')}</h5>
                        </div>

                        <div className = "container">
	                        <div className = "row">
	                        {this.state.members.map((member, index) => {
	                                	return(	 
                                            <li key={index}>                            					                                
                                            <div className="col-sm-3">
                                                <div className="team_item team_skill">
                                                    <div className="team_author">
                                                        <h4>{member.nombre}</h4>
                                                        <strong>{member.cargo}</strong>
                                                        <p>{member.estudio}</p>
                                                        <img className="img-circle img-team" src={member.foto} alt="" />
                                                    </div>

                                                    <div className="team_skill_title fix m-top-40 m-bottom-40">
                                                        <h5>{this.props.t('Better skills')}</h5>
                                                    </div>

                                                    <div className="skill_bar sm-m-top-50 m-top-20"> 
                                                        <div className="teamskillbar clearfix m-top-20" data-percent={member.habilidades[0].porcentaje}>
                                                            <h6 className="text-uppercase">{member.habilidades[0].habilidad}</h6>
                                                            <div className="teamskillbar-bar"></div>
                                                        </div>

                                                        <div className="teamskillbar clearfix m-top-50" data-percent={member.habilidades[1].porcentaje}>
                                                            <h6 className="text-uppercase">{member.habilidades[1].habilidad}</h6>
                                                            <div className="teamskillbar-bar"></div>
                                                        </div> 

                                                        <div className="teamskillbar clearfix m-top-50" data-percent={member.habilidades[2].porcentaje}>
                                                            <h6 className="text-uppercase">{member.habilidades[2].habilidad}</h6>
                                                            <div className="teamskillbar-bar"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <hr></hr>
                                            </div>
                                        </li>   
	                                	);
	                                })}
	                        </div>
	                      
	                    </div>
	                </div>
                </div>
            </section>
		)
	}
}

export default Team;
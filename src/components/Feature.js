import React, {Component} from 'react';

class Feature extends Component{
	render(){
		return(
			<section id="features" className="features bg-grey">
                <div className="container">
                    <div className="row">
                        <div className="main_features fix roomy-70">
                            <div className="col-md-6">
                                <div className="features_item sm-m-top-30">

                                <div className="logo" >
                            <img src="assets/images/codificacion.png" className="logo" alt=""/>
                            <br/>
                        </div>
                        
                                     <div className="f_item_text">
                                        <h3>{this.props.t('Web development')}</h3>
                                        <p>{this.props.t('Web development Description')}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="features_item sm-m-top-30">
                                    <div className="logo" >
                            <img src="assets/images/telefono-inteligente.png" className="logo" alt=""/>
                            <br/>
                        </div>
                                    <div className="f_item_text">
                                        <h3>{this.props.t('Mobile Development')}</h3>
                                        <p>{this.props.t('Mobile Development Description')}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="features_item m-top-70">
                                    <div className="logo" >
                            <img src="assets/images/sincronizar.png" className="logo" alt=""/>
                            <br/>
                        </div>

                                    <div className="f_item_text">
                                        <h3>{this.props.t('Infrastructure')}</h3>
                                        <p>{this.props.t('Infrastructure Description')}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="features_item m-top-70">
                                    <div className="logo" >
                            <img src="assets/images/analitica.png" className="logo" alt=""/>
                            <br/>
                        </div>
                                    <div className="f_item_text">
                                        <h3>{this.props.t('analysis of data')}</h3>
                                        <p>{this.props.t('analysis Description')}</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
		);
	}
}

export default Feature;
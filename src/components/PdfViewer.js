import React, { Component } from 'react';
import PDFViewer from 'pdf-viewer-reactjs';
 
const PdfViewer = (props) => {
    console.log(props.location.state.language);
    var language = props.location.state.language != undefined ? props.location.state.language: "es";
    switch(language){
        case "es":
            return (
                <PDFViewer
                document={{
                    url: 'http://localhost:5000/es',
                }}
            />
            );
        case "en":
            return (
                <PDFViewer
                document={{
                    url: 'http://localhost:5000/en',
                }}
            />
            ); 
            
    }
}
 
export default PdfViewer;
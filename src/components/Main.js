import React, { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';
import Navigation from './Navigation';
import Home from './Home';
import Feature from './Feature';
import Dialog from './Dialog';
import Product from './Product';
import Team from './Team';
import Clients from './Clients';
import Footer from './Footer';
import i18n from '../i18n/i18n';
import { withNamespaces } from 'react-i18next';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "es",
    }
  }
 
  changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
    this.setState({language: lng })
    console.log("lenguaje:"+ this.state.language);
  }
  render() {
    const { t } = this.props;

    return (
    <div>  
        <Navigation changeLanguage={this.changeLanguage} t={ t } language={this.state.language}/>
        <Home t={t}/>
        <Feature t={t}/>
        <Dialog t={t}/>
        <Product t={t}/>
        <Team t={t}/>
        <Clients t={t}/>
        <Footer t={t}/>
     </div>
    );
  }
}

export default  withNamespaces()(Main);
import React, {Component} from 'react';

class Dialog extends Component{
	render(){
		return(
			<section id="dialogue" className="dialogue bg-white roomy-80">
                <div className="container">
                    <div className="row">
                        <div className="main_dialogue text-center">
                            <div className="col-md-12">
                                <h3>{this.props.t('Phrase Tag 1')}
                                    <br/>{this.props.t('Phrase Tag 2')}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		)
	}
}

export default Dialog;
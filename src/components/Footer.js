
import React, {Component} from 'react';

class Footer extends Component{
	render(){
		return(
			<footer id="contact" className="action-lage bg-black p-top-80">
                <div className="container">
                    <div className="row">
                        <div className="widget_area">
                            <div className="col-md-3">
                                <div className="widget_item widget_about">
                                    <h5 className="text-white">{this.props.t('About us')}</h5>
                                    <p className="m-top-20 text-white">{this.props.t('About us description')}</p>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="widget_ab_item">
                                    
                                    <div className="widget_ab_item_text">
                                        <i className="fa fa-location-arrow"></i>
                                        <h6 className="text-white">{this.props.t('Location')}</h6>
                                        <p className="text-white">
                                            Armenia, Quindío, Colombia</p>
                                    </div>
                                </div>
                                <div className="widget_ab_item">
                                    
                                    <div className="widget_ab_item_text">
                                        <i className="fa fa-phone"></i>
                                        <h6 className="text-white">{this.props.t('Telephone')}</h6>
                                        <p className="text-white">+57 322 597 1957</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3">
                                    <div className="widget_ab_item">
                                        <div className="widget_ab_item_text">
                                            <i className="fa fa-envelope-o"></i>
                                            <h6 className="text-white">{this.props.t('Email')}</h6>
                                            <p className="text-white">business@devapps.com.co</p>
                                        </div>
                                    </div>
                                    <ul className="list-inline m-top-30">
                                        <li><a href="http://www.facebook.com/devappscolombia" target = "_blank"><i className="fa fa-facebook icon-footer"></i></a></li>
                                        <li><a href="http://www.instagram.com/devappscolombia" target = "_blank"><i className="fa fa-instagram icon-footer"></i></a></li>
                                    </ul>
                            </div>
                            <div className="col-md-3">
                                <div className="widget_item widget_newsletter sm-m-top-50">
                                    <div className="widget_brand">
                                        <img src="assets/images/logoblanco.png" className="logo-footer" alt=""/>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main_footer fix bg-mega text-center p-top-40 p-bottom-30 m-top-80">
                    <div className="col-md-12">
                        <p className="wow fadeInRight" data-wow-duration="1s">{this.props.t('Devapps Rights')}</p>
                    </div>
                </div>
            </footer>
		)
	}
}

export default Footer;
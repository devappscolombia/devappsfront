import React, { Component } from 'react';
import './App.css';
import Main from './components/Main';
import PdfViewer from './components/PdfViewer';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "es",
    }
  }
  
  render() {
    const { t } = this.props;

    return ( 
      <div>      
        <Router>
            <Switch>
              <Route path="/" component={Main} exact />
              <Route path="/portafolio" component={PdfViewer} />
            </Switch>
      </Router>
      </div>
    );
  }
}

export default App;